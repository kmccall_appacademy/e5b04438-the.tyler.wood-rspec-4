class Book

  attr_accessor :title

  def title=(t)
    @title = format_title(t)
  end

  private

  def format_title(title)
    words = title.split(" ")
    small_words = ["a","an","to","in","and","the","of"]
    words = words.map { |word| !small_words.include?(word) ? word.capitalize : word }
    words[0].capitalize!
    words.join(" ")
  end

end
