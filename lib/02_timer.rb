class Timer
  attr_accessor :seconds
  def initialize
    @seconds = 0
  end

  def time_string
    hours = @seconds / 3600
    minutes = (@seconds % 3600) / 60
    seconds = @seconds % 60

    hour_holder = ""
    minute_holder = ""
    second_holder = ""

    hours.to_s.length < 2 ? hour_holder = "0#{hours}" : hour_holder = "#{hours}"
    minutes.to_s.length < 2 ? minute_holder = "0#{minutes}" : minute_holder = "#{minutes}"
    seconds.to_s.length < 2 ? second_holder = "0#{seconds}" : second_holder = "#{seconds}"


    "#{hour_holder}:#{minute_holder}:#{second_holder}"
  end
end
