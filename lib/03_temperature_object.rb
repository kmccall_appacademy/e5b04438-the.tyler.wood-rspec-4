class Temperature
  attr_accessor :fahrenheit_temp

  def initialize(options_hash)
    options_hash.keys[0] == :f ? @fahrenheit_temp = options_hash[:f] :
                             @fahrenheit_temp = ctof(options_hash[:c])
  end

  def in_fahrenheit
    @fahrenheit_temp
  end

  def in_celsius
    ftoc(@fahrenheit_temp)
  end

  def self.from_celsius(num)
    self.new(:c => num)
  end

  def self.from_fahrenheit(num)
    self.new(:f => num)
  end

  private

  def ftoc(ftemp)
    (ftemp - 32.0) * 5/9.0
  end

  def ctof(ctemp)
    (ctemp * 9/5.0) + 32.0
  end

end

class Celsius < Temperature
  attr_accessor :ctemp

  def initialize(num)
    @ctemp = num
  end

  def in_celsius
    @ctemp
  end

  def in_fahrenheit
    ctof(@ctemp)
  end
end

class Fahrenheit < Temperature
  attr_accessor :ftemp

  def initialize(num)
    @ftemp = num
  end

  def in_celsius
    ftoc(@ftemp)
  end

  def in_fahrenheit
    @ftemp
  end
end
