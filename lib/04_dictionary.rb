class Dictionary

  def initialize
    @d = Hash.new("")
  end

  def add(input)
    if input.is_a?(String)
      @d[input] = nil
    else
      @d[input.keys[0]] = input.values[0]
    end
  end

  def entries
    @d
  end

  def keywords
    @d.keys.sort
  end

  def include?(input)
    @d.keys.include?(input)
  end

  def find(input)
    @d.select{ |k, v| k.include?(input) }
  end

  def printable
    result = []
    @d.sort.each do |k, v|
      result.push("[#{k}] \"#{v}\"")
    end
    result.join("\n")
  end

end
